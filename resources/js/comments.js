$(document).ready(function () {
    $('#create-comment-btn').click(function (event) {
        event.preventDefault();
        const data = $('#create-comment').serialize();
        const imageId = $('#image_id').val();

        $.ajax({
            url: `/images/${imageId}/comments`,
            method: "POST",
            data: data
        })
            .done(function (msg) {
                renderData(msg.comment);
            })
            .fail(function (response) {
            });
    });

    function renderData(comment) {
        let commentsBlock = $('.scrollit');
        $(commentsBlock).append(comment);
        clearForm();
    }

    function clearForm() {
        $("#create-comment").trigger('reset');
    }
});
