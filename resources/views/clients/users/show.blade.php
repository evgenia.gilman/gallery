@extends('layouts.app')
@section('content')

    <h2><b>@lang('messages.profile')</b> {{$user->name}}
        @can('edit', App\Models\User::class)
            @if($user->id == Auth::user()->id)
        <a href="{{route('users.edit', ['user'=> $user])}}" class="btn btn-primary btn-sm active" role="button"
           aria-pressed="true">@lang('messages.edit')</a>
            @endif
        @endcan
    </h2>

    <h5>@lang('messages.photos'):
        @can('create', App\Models\Image::class)
            @if($user->id == Auth::user()->id)
                <a href="{{route('images.create')}}" class="btn btn-primary btn-sm active" role="button"
                   aria-pressed="true">@lang('messages.create_new_photo')</a>
            @endif
        @endcan
    </h5>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">@lang('messages.photo')</th>
            <th scope="col">@lang('messages.title')</th>
            <th scope="col">@lang('messages.action')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($images as $image)
            <tr>
                @if($image->user_id == $user->id)
                    <td>
                        <a href="{{route('images.show', ['image' => $image])}}">
                        <img src="{{asset('/storage/' . $image->image)}}" class="card-img-top"
                             alt="{{asset('/storage/' . $image->image)}}" width="50" height="100%">
                        </a>
                    </td>
                    <td>{{$image->title}}</td>
                    <td>
                        @can('delete', $image)
                            <form style="float: left; margin-right: 12px;" action="{{route('images.destroy', ['image' => $image])}}" method="post">
                                @method('delete')
                                @csrf
                                <button class="btn btn-link">@lang('messages.remove')</button>
                            </form>
                        @endcan
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

