@extends('layouts.app')
@section('content')
    <div>
        <h3>@lang('messages.edit')</h3>
    </div>
    <form enctype="multipart/form-data" method="post" action="{{route('users.update', ['user' => $user])}}">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="name">@lang('messages.name')</label>
            <input type="text" class="form-control border-success  @error('name') is-invalid border-danger @enderror"
                   id="name" name="name" value="{{$user->name}}">
        </div>
        @error('name')
        <p class="text-danger">{{ $message }}</p>
        @enderror
        <button type="submit" class="btn btn-outline-primary">@lang('messages.save')</button>
    </form>
@endsection
