@extends('layouts.app')
@section('content')

    <div class="row">
        @foreach($images as $image)
            <div class="col" style="padding: 35px 0 0 0;" >
                <div class="card" style="width: 18rem; height: 100%" >
                    <div style="height: 200px">
                        <a href="{{route('images.show', ['image' => $image])}}">
                            <img src="{{asset('/storage/' . $image->image)}}" class="card-img"
                                 alt="{{asset('/storage/' . $image->imag)}}" height="100%">
                        </a>
                    </div>
                    <div class="card-body d-flex flex-column">
                        <h5 class="card-title"><b>{{$image->title}}</b></h5>
                    </div>
                    <div class="card-footer">
                        <small>@lang('messages.author'): <a href="{{route('users.show', ['user' => $image->user])}}">{{$image->user->name}}</a></small>
                    </div>
                </div>
            </div>
        @endforeach
    </div>


    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $images->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection
