@extends('layouts.app')
@section('content')
    <div>
        <h3>@lang('messages.create_new_photo')</h3>
    </div>
    <form enctype="multipart/form-data" method="post" action="{{route('images.store')}}">
        @csrf
        <div class="form-group">
            <label for="title">@lang('messages.title')</label>
            <input type="text" class="form-control border-success  @error('title') is-invalid border-danger @enderror"
                   id="title" name="title">
        </div>
        @error('title')
        <p class="text-danger">{{ $message }}</p>
        @enderror
        <div class="form-group">
            <div class="custom-file">
                <label for="customFile">@lang('messages.choose_file')</label>
                <input type="file" class="custom-file-input @error('image') is-invalid border-danger @enderror"
                       id="customFile" name="image">
                <label class="custom-file-label" for="customFile">@lang('messages.choose_file')</label>
            </div>
        </div>
        @error('image')
        <p class="text-danger">{{ $message }}</p>
        @enderror
        <button type="submit" class="btn btn-outline-primary">@lang('messages.save')</button>
    </form>
@endsection
