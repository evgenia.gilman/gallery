@extends('layouts.app')
@section('content')

    <div>
        <h3><b>{{$image->title}}</b></h3>
        <div class="card" style="width: 18rem;">
            <img src="{{asset('/storage/' . $image->image)}}" class="card-img-one"
                 alt="{{asset('/storage/' . $image->imag)}}">
        </div>
    </div>


    <div>
        <p>@lang('messages.average_rating'): {{round($rating, 1)}}</p>
    </div>

    <div id="accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne"
                            aria-expanded="true" aria-controls="collapseOne">@lang('messages.comments')
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <div class="row">
                        <div class="col-8 scrollit">
                            @foreach($image->comments as $comment)
                                <div class="">
                                    <div class="media g-mb-30 media-comment">
                                        <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
                                            <div class="g-mb-15">
                                                <h5 class="h5 g-color-gray-dark-v1 mb-0">{{$comment->user->name}}</h5>
                                                <span
                                                    class="g-color-gray-dark-v4 g-font-size-12">{{$comment->created_at->diffForHumans()}}</span>
                                            </div>
                                            <p>
                                                {{$comment->rating}}
                                            </p>
                                            <p>
                                                {{$comment->text}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(Auth::check())
            <div class="col-4 fixed">
                <div class="comment-form">
                    <form id="create-comment">
                        @csrf
                        <input type="hidden" id="image_id" value="{{$image->id}}">
                        <div class="form-group">
                            <label for="score">@lang('messages.rating')</label>
                            <select class="custom-select" name="rating" required>
                                <option value="">@lang('messages.choose_one')</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="commentFormControl">@lang('messages.comment')</label>
                            <textarea name="text" class="form-control" id="commentFormControl" rows="3"
                                      required></textarea>
                        </div>
                        <button id="create-comment-btn" type="submit" class="btn btn-outline-primary btn-sm btn-block">
                            @lang('messages.add_new_comment')
                        </button>
                    </form>
                </div>
            </div>
    </div>
    @endif

@endsection

