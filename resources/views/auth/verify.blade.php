@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> @lang('messages.checking_address')</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                @lang('messages.new_link')
                            </div>
                        @endif

                        @lang('messages.check_for_a_link')
                        @lang('messages.no_letter'),
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">@lang('messages.request_another')</button>.
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
