<?php

namespace App\Orchid\Screens;

use App\Models\Image;
use App\Orchid\Layouts\ImageListLayout;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class ImageListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'ImageListScreen';

    /**
     * @var string
     */
    public $description = 'All images';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'images' => Image::paginate()
        ];

    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new')
                ->icon('icon-pencil')
                ->route('platform.images.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            ImageListLayout::class
        ];
    }
}
