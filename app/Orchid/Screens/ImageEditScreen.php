<?php

namespace App\Orchid\Screens;

use App\Models\Image;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Screen;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Alert;

class ImageEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'ImageEditScreen';

    /**
     * @var string
     */
    public $description = 'ImageEditScreen';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Image $image
     * @return array
     */
    public function query(Image $image): array
    {
        $this->exists = $image->exists;

        if($this->exists){
            $this->name = 'Edit photo';
        }

        return [
            'image' => $image
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [

            Button::make('Create image')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),


            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [

            Layout::rows([

                Input::make('image.title')
                    ->title('Title')
                    ->placeholder('Attractive but mysterious title')
                    ->help('Specify a short descriptive title for this photo.'),


                Relation::make('image.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),

                Input::make('image.image')
                    ->title('Photo')
                    ->placeholder('Attractive but mysterious photo')
                    ->help('Example (images/f007ad3cdbe3cc541dac6f4acd0f17d5.jpg)')
            ])
        ];
    }

    /**
     * @param Image $image
     * @param Request $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Image $image, Request $request)
    {
        $image->fill($request->get('image'))->save();
        Alert::info('You have successfully created an photo.');
        return redirect()->route('platform.images.list');
    }

    /**
     * @param Image $image
     * @return RedirectResponse
     */
    public function remove(Image $image)
    {
        $image->delete()
            ? Alert::info('You have successfully deleted the photo.')
            : Alert::warning('An error has occurred')
        ;
        return redirect()->route('platform.images.list');
    }

}
