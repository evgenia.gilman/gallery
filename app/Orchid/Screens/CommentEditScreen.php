<?php

namespace App\Orchid\Screens;

use App\Models\Comment;
use App\Models\Image;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CommentEditScreen';

    /**
     * @var string
     */
    public $description = 'ArticleEditScreen';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Comment $comment
     * @return array
     */
    public function query(Comment $comment): array
    {
        $this->exists = $comment->exists;
        if($this->exists){
            $this->name = 'Edit comment';
        }

        return [
            'comment' => $comment
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create comment')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }


    /**
     * @return array
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('comment.text')
                    ->title('Text')
                    ->placeholder('Attractive but mysterious text')
                    ->help('Specify a short descriptive title for this comment.'),


                Relation::make('comment.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),

                Relation::make('comment.image_id')
                    ->title('Photo')
                    ->fromModel(Image::class, 'title', 'id'),

               Select::make('comment.rating')
                   ->title('Rating')
                   ->options([
                       1  => 1,
                       2  => 2,
                       3  => 3,
                       4  => 4,
                       5  => 5,
                   ]),

            ])

        ];
    }

    /**
     * @param Comment $comment
     * @param Request $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('You have successfully created an comment.');
        return redirect()->route('platform.comments.list');
    }

    public function remove(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('You have successfully deleted the comment.')
            : Alert::warning('An error has occurred')
        ;
        return redirect()->route('platform.comments.list');

    }
}
