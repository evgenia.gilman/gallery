<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Image;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ImagesCommentsController extends Controller
{
    /**
     * @param Request $request
     * @param Image $image
     * @return JsonResponse
     */
    public function store(Request $request, Image $image): JsonResponse
    {
        $comment = new Comment();
        $comment->text = $request->input('text');
        $comment->rating = $request->input('rating');
        $comment->user()->associate($request->user());
        $comment->image_id = $image->id;
        $comment->save();
        return response()->json(['comment' => view('comments.comment', compact('comment'))->render()], 201);
    }

}
