<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function show(User $user)
    {
        $images = Image::all();
        return view('clients.users.show', compact('user', 'images'));
    }


    /**
     * @param User $user
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('edit', User::class);
        return view('clients.users.edit', compact('user'));
    }


    /**
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(Request $request, User $user): RedirectResponse
    {
        $user->name = $request->input('name');
        $user->update();
        return redirect()->route( 'users.show', ['user' => $user]);
    }

}
