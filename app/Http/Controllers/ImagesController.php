<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequest;
use App\Models\Image;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ImagesController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $images = Image::orderBy('id', 'desc')
            ->paginate(9);

        return view('clients.images.index', compact('images'));
    }


    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Image::class);
        return  view('clients.images.create');
    }


    /**
     * @param ImageRequest $request
     * @return RedirectResponse
     */
    public function store(ImageRequest $request): RedirectResponse
    {
        $image = new Image();
        $image->title = $request->input('title');
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $path = $file->store('images', 'public');
            $image->image = $path;
        }
        $image->user()->associate($request->user());
        $image->save();

        return redirect()->route('users.show',$image->user_id);
    }


    /**
     * @param Image $image
     * @return Application|Factory|View
     */
    public function show(Image $image)
    {
        if($image->comments->count() !== 0) {
            foreach ($image->comments as $comment) {
                $ratings[] = $comment['rating'];
            }
            $rating  = collect($ratings)->avg();

        } else {
            $rating = 0;
        }

        return view('clients.images.show', compact('image', 'rating'));
    }


    /**
     * @param Image $image
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Image $image): RedirectResponse
    {
        $this->authorize('delete', $image);
        $image->delete();
        return redirect()->route('users.show',$image->user_id);
    }
}
