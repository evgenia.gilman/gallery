<?php

namespace App\Policies;

use App\Models\Image;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ImagePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->id;
    }

    /**
     * @param User $user
     * @param Image $image
     * @return bool
     */
    public function delete(User $user, Image $image)
    {
        return $user->id === $image->user->id;
    }
}
