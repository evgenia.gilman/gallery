<?php

use App\Http\Controllers\ImagesCommentsController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('language')->group(function() {

    Route::get('/', [ImagesController::class, 'index'])->name('home');
    Route::resource('images', ImagesController::class)->only(['index','show', 'create', 'store', 'destroy']);
    Route::resource('users', UsersController::class)->only(['show', 'edit', 'update']);
    Route::resource('images.comments', ImagesCommentsController::class)->only(['store'])
        ->middleware('auth');

    Auth::routes();

    });

    Route::get('language/{locale}', [\App\Http\Controllers\LanguageSwitcherController::class, 'switcher'])
        ->name('language.switcher')
        ->where('locale', 'en|ru');




