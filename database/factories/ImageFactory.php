<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Image::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(),
            'image' => $this->getImage(rand(1, 20)),
        ];
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage($image_number = 1): string
    {
        $path = storage_path() . "/seed_images/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = \Intervention\Image\Facades\Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('images/' . $image_name, $resize->__toString());
        return 'images/' . $image_name;
    }
}
