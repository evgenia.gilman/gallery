<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::all()->each(function ($user) {
            $images_count = rand(5, 15);
            $comment_count = rand(3, 8);
            \App\Models\Image::factory()
                ->for(
                    $user
                )->has(
                    \App\Models\Comment::factory()
                        ->for(
                            \App\Models\User::where('id', '!=', $user->id)->first()
                        )->count($comment_count)
                )
                ->count($images_count)->create();
        });
    }
}
